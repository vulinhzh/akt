package data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * thuật giải Akt
 */
@Data
public class AktAlgorithm {

    // tất cả nút
    private List<Node> nodes = new ArrayList<>();
    // các nút con của nút N
    private List<Node> childNodes = new ArrayList<>();
    // tập nút MO
    private List<Node> open = new ArrayList<>();
    private Node startNode;
    // đích
    private List<Node> goal = new ArrayList<>();
    //kết quả tìm kiếm
    private Node resultSearch;
    //chi phí tìm kiếm
    private Integer g = 0;


    public void addToOpen(Node node) {
        open.add(node);
    }

    public void removeFromOpen(Node n) {
        open.remove(n);
    }

    public List<Node> getMinFnInOpen() {
        List<Node> res = new ArrayList<>();
        Integer min = Integer.MAX_VALUE;
        for (Node node : open) {
            if (node.getFn() < min) {
                min = node.getFn();
            }
        }
        for (Node node : open) {
            if (node.getFn().equals(min))
                res.add(node);
        }
        return res;
    }

    public void updateChildNodeBn(Node nodeN) {
        childNodes.clear();
        for (Node node : nodes) {
            //todo nút cha cần để id cha là gì
            if (node.getParent().getId().equals(nodeN.getId()) && !node.getId().equals(nodeN.getId())) {
                node.setG(nodeN.getG() + node.getCost());
                node.setFnWithG(nodeN.getG());
                childNodes.add(node);
            }
        }
        open.addAll(childNodes);
    }

    public String getChildNodesString() {
        StringBuilder res = new StringBuilder();
        for (Node childNode : childNodes) {
            res.append(childNode.toString());
        }
        return res.toString();
    }

    public String getOpenNodesString() {
        StringBuilder res = new StringBuilder();
        for (Node childNode : open) {
            res.append(childNode.toString());
        }
        return res.toString();
    }

    public Boolean isGoal(Node n) {
        for (Node node : goal) {
            if (node.getId().equals(n.getId()))
                return true;
        }
        return false;
    }

}
