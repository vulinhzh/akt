package data;

import lombok.Data;

/**
 * Nút trên cây
 */
@Data
public class Node {
    private Integer id;
    private String name;
    //nút cha
    private Node parent;
    // chi phí tương lai
    private Integer h;
    // chi phí từ nút cha tới nút
    private Integer cost;
    // chi phí quá khứ
    private Integer g;
    // f(n)
    private Integer fn;

    public String getNodeTitle() {
        return id + "-" + name;
    }

    @Override
    public String toString() {
        return id + "-" + name + " (f=" + fn + "), ";
    }

    public void setFnWithG(Integer g) {
        fn = g + h + cost;
    }
}
