package data;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Item {
    private Integer id;
    private String title;

    @Override
    public String toString() {
        return this.title;
    }
}
