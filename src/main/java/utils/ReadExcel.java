package utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReadExcel {
    public static int[][] readExcel(int numRow, String path) {
        try {
            int[][] res = new int[numRow][numRow];
            File file = new File(path);   //creating a new file instance
            FileInputStream fis = new FileInputStream(file);   //obtaining bytes from the file

            //creating Workbook instance that refers to .xlsx file
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            XSSFSheet sheet = wb.getSheetAt(0);     //creating a Sheet object to retrieve object
            Iterator<Row> itr = sheet.iterator();    //iterating over excel file
            List<Row> listRow = new ArrayList<>();
            while (itr.hasNext()) listRow.add(itr.next());
            for (int i = 1; i < listRow.size(); ++i) {
                Row row = listRow.get(i);
                List<Cell> listCell = new ArrayList<>();
                Iterator<Cell> cellIterator = row.cellIterator();//iterating over each column
                while (cellIterator.hasNext()) listCell.add(cellIterator.next());
                for (int j = 1; j < listCell.size(); ++j) {
                    Cell cell = listCell.get(j);
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:    //field that represents string cell type
                            res[i-1][j-1] = Integer.valueOf(cell.getStringCellValue());
                            break;
                        case Cell.CELL_TYPE_NUMERIC:    //field that represents number cell type
                            res[i-1][j-1] = (int) cell.getNumericCellValue();
                            break;
                        default:
                    }
                }
            }
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}  