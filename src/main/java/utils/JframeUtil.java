package utils;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class JframeUtil {

    // bật tắt gui trước đó
    public static <T extends JFrame> void navigateGui(JFrame oldJframe, T newJframe) {
        oldJframe.setVisible(false);
    }

    // khởi tạo jframe
    public static void initJframe(JFrame jFrame, String name, Dimension dimension, JPanel jPanel) {
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        jFrame.setName(name);
        jFrame.setSize(dimension);
        jFrame.setLocationRelativeTo(null);
        jFrame.setContentPane(jPanel);
    }

    public static void showMessage(JFrame jframe, int i) {
        if (i != 0) JOptionPane.showMessageDialog(jframe,
                "Thao tác thành công !");
        else JOptionPane.showMessageDialog(jframe,
                "Vui lòng kiểm tra lại dữ liệu",
                "Thao tác thất bại",
                JOptionPane.ERROR_MESSAGE);
    }

    public static void showMessage(JFrame jframe, String message) {
        JOptionPane.showMessageDialog(jframe, message);
    }

    public static int showConfirm(JFrame jFrame, String message) {
        return JOptionPane.showConfirmDialog(jFrame, message);
    }
}
