package gui;

import data.AktAlgorithm;
import config.constant.JframeConstant;
import utils.JframeUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FunctionGUI extends JFrame {
    private JPanel function;
    private JButton btnEdit;
    private JButton btnSearch;
    private JButton btnViewDiag;
    private JButton btnAbout;

    public FunctionGUI(AktAlgorithm algorithm) {
        super();
        JframeUtil.initJframe(this, "Chức năng", JframeConstant.JFRAME_SIZE_FORM, function);
        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new NodeDataGUI(algorithm);
            }
        });
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SearchingGUI(algorithm);
            }
        });
        btnViewDiag.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TreeDiagram(algorithm);
            }
        });
        btnAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AboutGUI();
            }
        });
    }
}
