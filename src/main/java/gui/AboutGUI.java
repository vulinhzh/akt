package gui;

import config.constant.JframeConstant;
import utils.JframeUtil;

import javax.swing.*;

public class AboutGUI extends JFrame {
    private JPanel panel1;
    private JLabel icon;

    public AboutGUI() {
        super();
        JframeUtil.initJframe(this, "about", JframeConstant.JFRAME_SIZE_VIEW, panel1);
        ImageIcon imgThisImg = new ImageIcon("data\\icon.png");
        icon.setIcon(imgThisImg);
    }
}
