package gui;

import data.AktAlgorithm;
import config.constant.JframeConstant;
import data.Item;
import data.Node;
import utils.JframeUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchingGUI extends JFrame {
    private JPanel searching;
    private JTextField txtGoal;
    private JButton btnSearch;
    private JTable tbResult;
    private JComboBox cbNode;
    private Map<Integer, Node> nodeMap = new HashMap<>();

    public SearchingGUI(AktAlgorithm algorithm) {
        super();
        JframeUtil.initJframe(this, "Tìm đường", JframeConstant.JFRAME_SIZE_VIEW, searching);
        algorithm.getNodes().forEach(node -> {
            nodeMap.put(node.getId(), node);
            cbNode.addItem(new Item().setId(node.getId()).setTitle(node.getNodeTitle()));
        });
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Item item = (Item) cbNode.getSelectedItem();
                String goal = txtGoal.getText();
                if (item == null || goal.equals("")) {
                    showMessage("Vui lòng kiểm tra lại dữ liệu nhập!");
                    return;
                }
                Node startNode = nodeMap.get(item.getId());
//                startNode.setParent(null);
                algorithm.setStartNode(startNode);
                algorithm.getGoal().clear();
                List<String> goalStrings = Arrays.stream(goal.split(",")).toList();
                for (String s : goalStrings) {
                    Node node = nodeMap.get(Integer.valueOf(s));
                    algorithm.getGoal().add(node);
                }
                Boolean aBoolean = searchWithAkt(algorithm);
                if (aBoolean) {
                    String resultDirection = getResultDirection(algorithm);
                    showMessage(resultDirection);
                } else showMessage("Không tìm được đường đi tới đích!");
            }
        });
    }

    void showMessage(String message) {
        JframeUtil.showMessage(this, message);
    }

    private Boolean searchWithAkt(AktAlgorithm algorithm) {

        List<Node> nodes = algorithm.getNodes();
        String[] header = new String[]{"Bước lặp", "N", "B(n)", "MO", "DONG"};
        Object[][] data = new Object[200][5];
        int numStep = 0;
        algorithm.getOpen().add(algorithm.getStartNode());
        algorithm.setG(0);
        algorithm.getStartNode().setG(0);
        algorithm.getStartNode().setFnWithG(algorithm.getG());
        data[numStep][0] = "Khởi tạo";
        data[numStep][3] = algorithm.getStartNode().toString();
        String close = "";
        while (!algorithm.getOpen().isEmpty()) {
            ++numStep;
            data[numStep][0] = numStep;
            Node n;
            // tìm min fn
            List<Node> minFnNode = algorithm.getMinFnInOpen();
            // gắn N = min fn
            if (minFnNode.size() == 1) {
                if (algorithm.isGoal(minFnNode.get(0))) {
                    algorithm.setG(algorithm.getG() + minFnNode.get(0).getCost());
                    algorithm.setResultSearch(minFnNode.get(0));
                    data[numStep][1] = "Đích";
                    this.tbResult.setModel(new DefaultTableModel(data, header));
                    return true;
                }
                n = minFnNode.get(0);
            } else {
                for (Node node : minFnNode) {
                    if (algorithm.isGoal(node)) {
                        algorithm.setG(algorithm.getG() + minFnNode.get(0).getCost());
                        algorithm.setResultSearch(node);
                        this.tbResult.setModel(new DefaultTableModel(data, header));
                        data[numStep][1] = "Đích";
                        return true;
                    }
                }
                n = minFnNode.get(0);
            }
            data[numStep][1] = n.getNodeTitle();
            //đóng đỉnh N
            algorithm.removeFromOpen(n);
            close += n.getNodeTitle() + ", ";
            data[numStep][4] = close;
            // lấy B(n)
            algorithm.updateChildNodeBn(n);
            String bn = "";
            String open = "";
            for (Node childNode : algorithm.getChildNodes()) {
                bn += childNode.getNodeTitle() + ", ";
            }
            for (Node openNode : algorithm.getOpen()) {
                open += openNode.toString();
            }
            data[numStep][2] = bn;
            data[numStep][3] = open;
            // update g
            algorithm.setG(algorithm.getG() + n.getCost());
        }
        data[numStep + 1][0] = "Không tìm thấy!";
        this.tbResult.setModel(new DefaultTableModel(data, header));
        return false;
    }

    private String getResultDirection(AktAlgorithm algorithm) {
        Integer cost = 0;
        String[] arr = new String[100];
        int i = -1;
        String result = "";
        Node rs = algorithm.getResultSearch();
        while (true) {
            ++i;
            arr[i] = rs.getName();
            cost += rs.getCost();
            if (rs.getId().equals(rs.getParent().getId())) break;
            rs = rs.getParent();
        }
        for (int j = i; j >= 0; j--) {
            if (j != 0) result += arr[j] + " -> ";
            else result += arr[j];
        }
        return "Đường đi: " + result + "\nChi phí: " + cost;
    }
}
