package gui;

import config.constant.JframeConstant;
import data.AktAlgorithm;
import data.Node;
import utils.JframeUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TreeDiagram extends JFrame {
    private JPanel diagram;
    private JButton btnOpen;
    private JButton btnExit;
    private JButton btnStruct;
    private JLabel labelDiagram;
    private AktAlgorithm algorithm;

    public TreeDiagram(AktAlgorithm algorithm) {
        super();
        JframeUtil.initJframe(this, "Đồ thị", JframeConstant.JFRAME_SIZE_FORM, diagram);
        this.algorithm = algorithm;
        List<Node> nodes = algorithm.getNodes().stream()
                .filter(node -> node.getParent() != null)
                .collect(Collectors.toList());
        Map<Integer, Node> mapNode = nodes.stream()
                .collect(Collectors.toMap(Node::getId, Function.identity()));
        algorithm.setNodes(nodes);
        // lay nut goc
        Node rootNode = getRootNode();
        // chia level
        List<List<Node>> levelList = getLevelList(algorithm, rootNode);
        // map nut va html
        Map<Integer, String> mapHtml = new HashMap<>();
        int level = levelList.size(); // start from 0
        for (int i = level - 1; i > 0; i--) {
            // map html cua nut
            Map<Integer, String> mapLevelHtml = new HashMap<>();
            List<Node> currLevel = levelList.get(i);
            for (Node node : currLevel) {
                if (!mapHtml.containsKey(node.getId()))
                    mapLevelHtml.put(node.getId(), formatNodeNotHaveChild(node));
                else mapLevelHtml.put(node.getId(), formatNodeHaveChild(node, mapHtml.get(node.getId())));
            }
            Map<Integer, List<String>> mapParentHtml = mapLevelHtml.entrySet().stream()
                    .collect(Collectors.groupingBy(e -> mapNode.get(e.getKey()).getParent().getId(),
                            Collectors.mapping(e -> e.getValue(), Collectors.toList())));
            mapParentHtml.entrySet().forEach(e -> {
                String html = "";
                List<String> value = e.getValue();
                for (String s : value) {
                    html = html.concat(s);
                }
                html = "<ul>" + html + "</ul>";
                mapHtml.put(e.getKey(), html);
            });
        }
        String rootHtml = formatNodeHaveChild(rootNode, mapHtml.get(rootNode.getId()));
        String html = getHtml(rootHtml);
        save(html);
        btnOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File fileToOpen = new File("tree.html");
                try {
                    Desktop.getDesktop().open(fileToOpen);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnStruct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TreeStructure(algorithm);
            }
        });
    }

    // chia các nút vào các cấp độ
    private List<List<Node>> getLevelList(AktAlgorithm algorithm, Node rootNode) {
        List<List<Node>> levelList = new ArrayList<>();
        //dem so nut da duoc ghep vao cay
        int numCounted = 1;
        levelList.add(List.of(rootNode));
        while (numCounted != algorithm.getNodes().size()) {
            List<Node> currentLevel = new ArrayList<>();
            int level = levelList.size();
            List<Node> preLevel = levelList.get(level - 1);
            for (Node node : algorithm.getNodes()) {
                if (node.getId().equals(rootNode.getId())) continue;
                if (preLevel.contains(node.getParent())) {
                    currentLevel.add(node);
                    numCounted++;
                }
            }
            levelList.add(currentLevel);
        }
        return levelList;
    }

    public Node getRootNode() {
        List<Node> nodes = algorithm.getNodes();
        for (Node node : nodes) {
            if (node.getParent().getId().equals(node.getId()))
                return node;
        }
        return null;
    }

    public String formatNodeNotHaveChild(Node node) {
        String format = "<li><span>%d-%s<p>g=%d h=%d</p></span></li>";
        return String.format(format, node.getId(), node.getName(), node.getCost(), node.getH());
    }

    public String formatNodeHaveChild(Node node, String child) {
        String format = "<li><span>%d-%s<p>g=%d h=%d</p></span>%s</li>";
        return String.format(format, node.getId(), node.getName(), node.getCost(), node.getH(), child);
    }

    public String getHtml(String rootHtml) {
        String s1 =
                "<html>\n" +
                        "\n" +
                        "<head>\n" +
                        "    <style>\n" +
                        "        .tree,\n" +
                        "        .tree ul,\n" +
                        "        .tree li {\n" +
                        "            list-style: none;\n" +
                        "            margin: 0;\n" +
                        "            padding: 0;\n" +
                        "            position: relative;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree {\n" +
                        "            margin: 0 0 1em;\n" +
                        "            text-align: center; margin-left:20%;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree,\n" +
                        "        .tree ul {\n" +
                        "            display: table;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree ul {\n" +
                        "            width: 100%;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree li {\n" +
                        "            display: table-cell;\n" +
                        "            padding: .5em 0;\n" +
                        "            vertical-align: top;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree li:before {\n" +
                        "            outline: solid 1px #666;\n" +
                        "            content: \"\";\n" +
                        "            left: 0;\n" +
                        "            position: absolute;\n" +
                        "            right: 0;\n" +
                        "            top: 0;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree li:first-child:before {\n" +
                        "            left: 50%;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree li:last-child:before {\n" +
                        "            right: 50%;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree code,\n" +
                        "        .tree span {\n" +
                        "            border: solid .1em #666;\n" +
                        "            border-radius: 1.2em;\n" +
                        "            display: inline-block;\n" +
                        "            margin: 0 .2em .5em;\n" +
                        "            padding: .2em .5em;\n" +
                        "            position: relative;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree ul:before,\n" +
                        "        .tree code:before,\n" +
                        "        .tree span:before {\n" +
                        "            outline: solid 1px #666;\n" +
                        "            content: \"\";\n" +
                        "            height: .5em;\n" +
                        "            left: 50%;\n" +
                        "            position: absolute;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree ul:before {\n" +
                        "            top: -.5em;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree code:before,\n" +
                        "        .tree span:before {\n" +
                        "            top: -.55em;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree>li {\n" +
                        "            margin-top: 0;\n" +
                        "        }\n" +
                        "\n" +
                        "        .tree>li:before,\n" +
                        "        .tree>li:after,\n" +
                        "        .tree>li>code:before,\n" +
                        "        .tree>li>span:before {\n" +
                        "            outline: none;\n" +
                        "        }\n" +
                        "\n" +
                        "        p {\n" +
                        "            margin: .1em;\n" +
                        "            color: rgb(235, 100, 51);\n" +
                        "        }\n" +
                        "\n" +
                        "        span {\n" +
                        "            background-color: #e7e9eb9f;\n" +
                        "\n" +
                        "        }\n" +
                        "    </style>\n" +
                        "</head>\n" +
                        "\n" +
                        "<body>\n" +
                        "    <ul class=\"tree\">";
        String s2 = "</ul>\n" +
                "\n" +
                "</body>\n" +
                "\n" +
                "</html>";
        return s1 + rootHtml + s2;
    }

    private void save(String html) {
        FileWriter fWriter = null;
        BufferedWriter writer = null;
        try {
            fWriter = new FileWriter("tree.html");
            writer = new BufferedWriter(fWriter);
            writer.write(html);
            writer.newLine(); //this is not actually needed for html files - can make your code more readable though
            writer.close(); //make sure you close the writer object
        } catch (Exception e) {
            //catch any exceptions here
        }
    }
}
