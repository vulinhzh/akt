package gui;

import config.constant.JframeConstant;
import data.AktAlgorithm;
import data.Node;
import utils.JframeUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

public class InitTree extends JFrame {

    private JPanel initTree;
    private JTextField txtNode;
    private JButton button1;
    private AktAlgorithm algorithm = new AktAlgorithm();

    public InitTree() {
        super();
        JframeUtil.initJframe(this, "Chuẩn bị", JframeConstant.JFRAME_SIZE_VIEW, initTree);
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String text = txtNode.getText();
                    List<String> names = Arrays.stream(text.split(",")).toList();
                    for (int i = 0; i < names.size(); i++) {
                        String name = names.get(i);
                        name = name.trim().toUpperCase();
                        Node node = new Node();
                        node.setId(i + 1);
                        node.setName(name);
                        algorithm.getNodes().add(node);
                    }
                    for (String name : names) {
                        if (name.equals(" ") || name.length() == 0)
                            throw new RuntimeException();
                    }
                    navigate(algorithm);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    showMessage("Có lỗi xảy ra, vui lòng kiểm tra lại dữ liệu nhập!");
                }
            }
        });
    }

    public void showMessage(String mess) {
        JframeUtil.showMessage(this, mess);
    }

    private void navigate(AktAlgorithm algorithm) {
        new FunctionGUI(algorithm);
        dispose();
    }
}
