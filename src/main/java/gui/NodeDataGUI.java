package gui;

import config.constant.JframeConstant;
import data.AktAlgorithm;
import data.Item;
import data.Node;
import utils.JframeUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NodeDataGUI extends JFrame {
    private JPanel taskGUI;
    private JTextField txtId;
    private JTextField txtName;
    private JTable data;
    private JButton btnUpdate;
    private JButton btnCancel;
    private JButton btnReset;
    private JComboBox cbNode;
    private JTextField txtH;
    private JTextField txtCost;
    private JComboBox cbParent;
    private AktAlgorithm algorithm;
    private Map<Integer, Node> mapNode = new HashMap<>();

    public NodeDataGUI(AktAlgorithm algorithm) {
        super();
        this.algorithm = algorithm;
        JframeUtil.initJframe(this, "Quản lý", JframeConstant.JFRAME_SIZE_VIEW, taskGUI);
        algorithm.getNodes().forEach(node -> {
            cbNode.addItem(new Item().setId(node.getId()).setTitle(node.getNodeTitle()));
            cbParent.addItem(new Item().setId(node.getId()).setTitle(node.getNodeTitle()));
            mapNode.put(node.getId(), node);
        });
        showAll();
        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer id = Integer.valueOf(txtId.getText());
                Node node = mapNode.get(id);
                if (!txtH.getText().equals(""))
                    node.setH(Integer.valueOf(txtH.getText()));
                if (!txtCost.getText().equals(""))
                    node.setCost(Integer.valueOf(txtCost.getText()));
                Item item = (Item) cbParent.getSelectedItem();
                node.setParent(mapNode.get(item.getId()));
                showAll();
            }
        });
        data.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int rowSelected = data.getSelectedRow();
                DefaultTableModel model = (DefaultTableModel) data.getModel();
                Object id = model.getValueAt(rowSelected, 0);
                Node node = mapNode.get((Integer) id);
                txtId.setText(id.toString());
                txtName.setText(node.getName());
                txtH.setText(node.getH() == null ? "" : node.getH().toString());
                txtCost.setText(node.getCost() == null ? "" : node.getH().toString());
                cbNode.setSelectedIndex(node.getId() - 1);
                cbParent.setSelectedIndex(node.getParent() == null ? 0 : node.getParent().getId() - 1);
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtId.setText("");
                txtName.setText("");
                txtH.setText("");
                txtCost.setText("");
                cbParent.setSelectedIndex(0);
                cbNode.setSelectedIndex(0);
            }
        });

        cbNode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Item item = (Item) cbNode.getSelectedItem();
                txtId.setText(item.getId().toString());
                txtName.setText(mapNode.get(item.getId()).getName());
            }
        });
    }

    private void showAll() {
        List<Node> nodes = algorithm.getNodes();
        String[] header = new String[]{"Id", "Tên", "Chi phí h(n)", "Nút cha", "Chi phí tới nút cha"};
        Object[][] data = new Object[nodes.size()][5];
        for (int i = 0; i < nodes.size(); ++i) {
            data[i][0] = nodes.get(i).getId();
            data[i][1] = nodes.get(i).getName();
            data[i][2] = nodes.get(i).getH() == null ? "undefine" : nodes.get(i).getH();
            data[i][3] = nodes.get(i).getParent() == null ? "undefine" : nodes.get(i).getParent().getName();
            data[i][4] = nodes.get(i).getCost() == null ? "undefine" : nodes.get(i).getCost();
        }
        this.data.setModel(new DefaultTableModel(data, header));
    }
}
