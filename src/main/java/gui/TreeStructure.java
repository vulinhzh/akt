package gui;

import config.constant.JframeConstant;
import data.AktAlgorithm;
import data.Node;
import utils.JframeUtil;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TreeStructure extends JFrame{
    private JPanel treeStructure;
    private JScrollPane scrollPane;
    private JTree jTree;

    public TreeStructure(AktAlgorithm algorithm) {
        super();
        JframeUtil.initJframe(this, "Đồ thị", JframeConstant.JFRAME_SIZE_VIEW, treeStructure);
        Map<Integer, DefaultMutableTreeNode> treeNode = algorithm.getNodes().stream()
                .collect(Collectors.toMap(Node::getId, node -> new DefaultMutableTreeNode(node.getNodeTitle())));
        Node root = new Node();
        for (Node node : algorithm.getNodes()) {
            if (node.getId().equals(node.getParent().getId())) root = node;
            else {
                DefaultMutableTreeNode tNode = treeNode.get(node.getId());
                for (Node child : algorithm.getNodes()) {
                    if (child.getParent().getId().equals(node.getId()))
                        tNode.add(treeNode.get(child.getId()));
                }
            }
        }
        for (Node child : algorithm.getNodes()) {
            if (child.getParent().getId().equals(root.getId()) && !child.getId().equals(child.getParent().getId()))
                treeNode.get(root.getId()).add(treeNode.get(child.getId()));
        }
//        jTree = new JTree(treeNode.get(root.getId()));
//        scrollPane.add(jTree);
        DefaultTreeModel defaultTreeModel = new DefaultTreeModel(treeNode.get(root.getId()));
        jTree.setModel(defaultTreeModel);
    }
}
