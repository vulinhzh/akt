package config.constant;

import java.awt.*;

public class JframeConstant {
    public static Dimension JFRAME_SIZE_FORM = new Dimension(600, 400);
    public static Dimension JFRAME_SIZE_VIEW = new Dimension(800, 600);
}
